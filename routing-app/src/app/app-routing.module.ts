import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './default/default.component';
import {FirstComponentComponent} from './first-component/first-component.component'
import {SecondComponentComponent} from './second-component/second-component.component'

const routes: Routes = [
  { path: 'first-component/:productId', component: FirstComponentComponent },
  { path: 'first-component', component: FirstComponentComponent },
  { path: 'second-component', component: SecondComponentComponent },
  { path: '**', component: DefaultComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
