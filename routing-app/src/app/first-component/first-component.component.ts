import { Component, OnInit } from '@angular/core';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-first-component',
  templateUrl: './first-component.component.html',
  styleUrls: ['./first-component.component.css']
})
export class FirstComponentComponent implements OnInit {

  constructor(  private route: ActivatedRoute,
    private router: Router) { }
  id = this.route.snapshot.paramMap.get('productId');
  ngOnInit(): void {
  }

}
